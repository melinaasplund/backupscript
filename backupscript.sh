#!/bin/bash

#skapa cli arguements
source_dir="$1"
target_dir="$2/$(date "+%Y-%m-%d_%H_%M_%S")"

latestdir=$(ls -td $target_dir | head -1)

#skapa ny backup mapp
mkdir $target_dir

cd $latestdir
ln * $target_dir/
cd .. 

#om lokalt, skriv in source_dir som path till lokal mapp
rsync -avEPhz $source_dir/* $target_dir/

#om remote, skirv in source_dir som en path till ssh ex (vagrant@127.0.0.1:/home/vagrant/source/*)
rsync -avEPhz -e 'ssh -p 2222' $source_dir/* $target_dir/